﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berndt
{
    public class Record
    {
        public DateTime Date { get; set; }
        public IPAddress ClientIp { get; set; }
        public string UserName { get; set; }
        public string ServiceName { get; set; }
        public string ServerName { get; set; }
        public string ServerIp { get; set; }
        public int ServerPort { get; set; }
        public string Method { get; set; }
        public string UriStem { get; set; }
        public string UriQuery { get; set; }
        public int ProtocolStatus { get; set; }
        public int Win32Status { get; set; }
        public int BytesSent { get; set; }
        public int BytesRecieved { get; set; }
        public int TimeTaken { get; set; }
        public string ProtocolVersion { get; set; }
        public string Host { get; set; }
        public string UserAgent { get; set; }
        public string Cookie { get; set; }
        public string Referer { get; set; }
    }
}
