﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berndt
{
    /// <summary>
    ///  IP Address implementation
    /// </summary>
    public class IPAddress : IComparable<IPAddress>
    {
        private int octet1;
        private int octet2;
        private int octet3;
        private int octet4;

        /// <summary>
        /// Initializes IpAddress from string
        /// </summary>
        /// <param name="ipStr">String format of ip</param>
        public IPAddress(string ipStr)
        {
            var octets = ipStr.Split('.');

            if(octets.Length == 4)
            {
                octet1 = Int32.Parse(octets[0]);
                octet2 = Int32.Parse(octets[1]);
                octet3 = Int32.Parse(octets[2]);
                octet4 = Int32.Parse(octets[3]);
            }
        }

        public override string ToString()
        {
            return string.Format("{0}.{1}.{2}.{3}", octet1, octet2, octet3, octet4);
        }

        public override bool Equals(object obj)
        {
            var other = obj as IPAddress;

            if (other == null) return false;

            return other.octet1 == octet1 && other.octet2 == octet2 && other.octet3 == octet3 && other.octet4 == octet4;
        }

        /// <summary>
        /// Provides a hash based on integer value
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return ConvertToInt();
        }

        /// <summary>
        /// Changes ip to integer
        /// </summary>
        /// <returns></returns>
        private int ConvertToInt()
        {
            return octet4 + (octet3 * 256) + (octet2 * 65536) + (octet1 * 16777216);
        }

        /// <summary>
        /// Compares one IP adderss to another by octets
        /// </summary>
        /// <param name="other">Object being compared to</param>
        /// <returns>1 if greater, 0 if equal -1 if less</returns>
        public int CompareTo(IPAddress other)
        {
            var c1 = octet1.CompareTo(other.octet1);
            if (c1 != 0) return c1;

            var c2 = octet2.CompareTo(other.octet2);
            if (c2 != 0) return c2;

            var c3 = octet3.CompareTo(other.octet3);
            
            return c3 != 0 ? c3 : octet4.CompareTo(other.octet4);
        }
    }
}
