﻿using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Berndt
{
    public class LogReporter
    {
        private const string RECORD_FILE_NAME = "report.csv";
        private const string EXCLUDE_IP = "207.114";
        private const int ALLOW_IP = 80; // Default http port
        private const string TARGET_METHOD = "GET";

        private List<Record> records;

        public LogReporter()
        {
            records = new List<Record>();
        }
        
        /// <summary>
        /// Reads file from path and saves to memory
        /// </summary>
        /// <param name="path">File path</param>
        public void ReadLogFile(string path)
        {
            if (File.Exists(path))
            {
                using (var reader = new StreamReader(path))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var items = line.Split(' ');

                        if (items.Length == 21)
                        {
                            records.Add(new Record()
                            {
                                Date = DateTime.Parse(string.Format("{0} {1}", items[0], items[1])),
                                ClientIp = new IPAddress(items[2]),
                                UserName = items[3],
                                ServiceName = items[4],
                                ServerName = items[5],
                                ServerIp = items[6],
                                ServerPort = Int32.Parse(items[7]),
                                Method = items[8],
                                UriStem = items[9],
                                UriQuery = items[10],
                                ProtocolStatus = Int32.Parse(items[11]),
                                Win32Status = Int32.Parse(items[12]),
                                BytesSent = Int32.Parse(items[13]),
                                BytesRecieved = Int32.Parse(items[14]),
                                TimeTaken = Int32.Parse(items[15]),
                                ProtocolVersion = items[16],
                                Host = items[17],
                                UserAgent = items[18],
                                Cookie = items[19],
                                Referer = items[20]
                            });
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Log file doesnt exist: " + path);
            }
        }

        /// <summary>
        /// Writes the report to CSV file
        /// </summary>
        public void WriteReport()
        {
            var sb = new StringBuilder();

            foreach (var record in records.Where(x => !x.ClientIp.ToString().StartsWith(EXCLUDE_IP) && x.ServerPort == ALLOW_IP && x.Method == TARGET_METHOD)
                .GroupBy(rec => rec.ClientIp)
                .Select(group => new
                {
                    Ip = group.Key,
                    Count = group.Count()
                }).OrderByDescending(x => x.Count).ThenByDescending(y => y.Ip))
            {
                sb.AppendFormat("{0}, \"{1}\"\n", record.Count, record.Ip);
            }

            // write file
            var filepath = string.Format("{0}\\{1}", Directory.GetCurrentDirectory(), RECORD_FILE_NAME);

            if (File.Exists(filepath)) File.Delete(filepath);
            File.WriteAllText(filepath, sb.ToString());

        }
    }
}
