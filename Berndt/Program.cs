﻿using System.IO;
using System;

namespace Berndt
{
    class Program
    {
        public const string FILENAME = "access.log";

        static void Main(string[] args)
        {
            var logReporter = new LogReporter();
            logReporter.ReadLogFile(string.Format("{0}\\{1}", Directory.GetCurrentDirectory(), FILENAME));
            logReporter.WriteReport();

            Console.WriteLine("Complete!");
            Console.ReadLine(); 

        }
    }
}
